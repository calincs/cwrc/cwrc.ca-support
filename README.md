# cwrc.ca supporting docker containers

An Docker-Compose orgistration of containers to support CWRC-Writer services including: NSSI/NERVE named entity recognition, CWRC-Validator service to validate XML, and a set of https endpoints for authorities such as Dbpedia and Getty.

## Contents

* NSSI/NERVE: <https://gitlab.com/calincs/conversion/NSSI>
  * production
  * uat
* CWRC Validation service: <https://github.com/cwrc/CWRC-Validator>
  * production
  * uat
* Set of HTTPS reverse proxies for dbpedia and Getty
* Traefik: ingress controller, HTTPS certificate management, and reverse proxy
* Watchtower to automatically update the included containers

## To run

* setup DNS entries for the routes defined in the Traefik config
* `docker-compose up -d`
